Metoda		Adres url							Opis
GET 		http://localhost:8080/samplerest/rest/film 		Pobranie listy filmow
POST  		http://localhost:8080/samplerest/rest/film 		Zapisanie nowego filmu
GET 		http://localhost:8080/samplerest/rest/film/{filmId} 	Pobranie filmu o podanym id 
PUT 		http://localhost:8080/samplerest/rest/film/{filmId} 	Aktualizacja danych filmu o podanym id
DELETE		http://localhost:8080/samplerest/rest/film/{filmId} 	UsuniÍcie filmu o danym id 
GET		http://localhost:8080/samplerest/rest/film/{filmId}/actors 
