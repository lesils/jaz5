	
	package domain.services;
	
	import java.util.ArrayList;
	import java.util.List;
	
	import domain.Actor;
	
	public class ActorService {
	
		private static List<Actor> db = new ArrayList<Actor>();
		private static int currentId = 1;
		public List<Actor> getAll(){
			return db;
		}
		
		public Actor get(int id){
			for(Actor p : db){
				if(p.getId()==id)
					return p;
			}
			return null;
		}
		
		public void add(Actor p){
			p.setId(++currentId);
			db.add(p);
		}
		
		public void update(Actor actor){
			for(Actor p : db){
				if(p.getId()==actor.getId()){
					p.setName(actor.getName());
					p.setSurname(actor.getSurname());
				}
			}
		}
		
		public void delete(Actor p){
			db.remove(p);
		}
		
	}

	
	