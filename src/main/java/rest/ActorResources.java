	
	package rest;
	
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



import domain.Film;
import domain.Actor;
import domain.services.ActorService;
	
	@Path("/actor")
	public class ActorResources {
	
		private ActorService db = new ActorService();
		
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public List<Actor> getAll()
		{
			return db.getAll();
		}
		
		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		public Response Add(Actor actor){
			db.add(actor);
			return Response.ok(actor.getId()).build();
		}
		
		@GET
		@Path("/{id}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response get(@PathParam("id") int id){
			Actor result = db.get(id);
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
		
		@PUT
		@Path("/{id}")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response update(@PathParam("id") int id, Actor p){
			Actor result = db.get(id);
			if(result==null)
				return Response.status(404).build();
			p.setId(id);
			db.update(p);
			return Response.ok().build();
		}
		@DELETE
		@Path("/{id}")
		public Response delete(@PathParam("id") int id){
			Actor result = db.get(id);
			if(result==null)
				return Response.status(404).build();
			db.update(result);
			return Response.ok().build();
		}
		
		
		
		@POST
		@Path("/{id}/actors")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response addActor(@PathParam("id") int actorId, Film film){
			Actor result = db.get(actorId);
			if(result==null)
				return Response.status(404).build();
			if(result.getFilm()==null)
				result.setFilm(new ArrayList<Actor>());
			return Response.ok().build();
		}
	}
	
	
	
	

	