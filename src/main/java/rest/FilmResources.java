package rest;

import domain.Actor;
import domain.Comment;
import domain.Film;
import domain.services.FilmService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/film")
public class FilmResources {

    private FilmService db = new FilmService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Film> getAll(){
        return db.getAll();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response Add(Film film){
        db.add(film);
        return Response.ok(film.getId()).build();

    }
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id){
        Film result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        return Response.ok(result).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") int id, Film f){
        Film result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        f.setId(id);
        db.update(f);
        return Response.ok(result).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") int id){
        Film result = db.get(id);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }

    @GET
    @Path("/{filmId}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Actor> getActors(@PathParam("filmId") int filmId){
        Film result = db.get(filmId);
        if(result==null){
            return null;
        }
        if (result.getActors()==null)
            result.setActors(new ArrayList<Actor>());
        return result.getActors();
    }

    @POST
    @Path("/{filmId}/actors")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addActor(@PathParam("filmId") int filmId, Actor actor){
        Film result = db.get(filmId);
        if(result==null){
            return Response.status(404).build();
        }
        if (result.getActors()==null)
            result.setActors(new ArrayList<Actor>());
        result.getActors().add(actor);
        return Response.ok().build();
    }

    @GET
    @Path("/{filmId}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Comment> getComments(@PathParam("filmId") int filmId){
        Film result = db.get(filmId);
        if(result==null){
            return null;
        }
        if (result.getComments()==null)
            result.setComments(new ArrayList<Comment>());
        return result.getComments();
    }

    @POST
    @Path("/{filmId}/comments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addComment(@PathParam("filmId") int filmId, Comment comment){
        Film result = db.get(filmId);
        if(result==null){
            return Response.status(404).build();
        }
        if (result.getComments()==null)
            result.setComments(new ArrayList<Comment>());
        result.getComments().add(comment);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{filmId}/comments/{commentId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("commentId") int filmId, int commentId){
        Film result = db.get(commentId);
        if(result==null){
            return Response.status(404).build();
        }
        db.update(result);
        return Response.ok(result).build();
    }
}
